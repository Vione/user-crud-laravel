@extends('layouts.app')

@section('title', 'Update user')

@section('content')
    <h2>@lang('Update user')</h2>
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    <form action="{{url('user/'.request()->route('id').'/modify')}}" class="form" method="post">
        @csrf
        <div class="form-group">
            <label for="full_name">@lang('Full name')</label>
            <input type="text"
                   class="form-control"
                   id="full_name"
                   name="full_name"
                   placeholder="@lang('Full name')"
                   value="{{$user->full_name}}">
        </div>
        <div class="form-group">
            <label for="email">@lang('Email')</label>
            <input type="email"
                   class="form-control"
                   id="email"
                   name="email"
                   placeholder="@lang('Email')"
                   value="{{$user->email}}">
        </div>
        <hr>
        <div class="form-group">
            <label for="password">@lang('Password')</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="@lang('Password')">
        </div>
        <button type="submit" class="btn btn-primary">@lang('Update user')</button>
    </form>
@endsection
