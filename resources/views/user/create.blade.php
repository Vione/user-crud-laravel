@extends('layouts.app')

@section('title', 'Create user')

@section('content')
    <h2>@lang('Create user')</h2>
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    <form action="{{url('user/store')}}" class="form" method="post">
        @csrf
        <div class="form-group">
            <label for="full_name">@lang('Full name')</label>
            <input type="text" class="form-control" id="full_name" name="full_name" placeholder="@lang('Full name')">
        </div>
        <div class="form-group">
            <label for="username">@lang('Username')</label>
            <input type="text" class="form-control" id="username" name="username" placeholder="@lang('Username')">
        </div>
        <div class="form-group">
            <label for="password">@lang('Password')</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="@lang('Password')">
        </div>
        <div class="form-group">
            <label for="password_confirmation">@lang('Password confirmation')</label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                   placeholder="@lang('Password confirmation')">
        </div>
        <div class="form-group">
            <label for="email">@lang('Email')</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="@lang('Email')">
        </div>
        <button type="submit" class="btn btn-primary">@lang('Create user')</button>
    </form>
@endsection
