@extends('layouts.app')

@section('title', 'User list')

@section('content')
    <h2>@lang('User list')</h2>
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('Full name')</th>
            <th>@lang('Email')</th>
            <th>@lang('Actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->full_name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{url("/user/{$user->id}/delete")}}" class="btn btn-danger">@lang('Delete')</a>
                    <a href="{{url("/user/{$user->id}/update")}}" class="btn btn-primary">@lang('Update')</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
