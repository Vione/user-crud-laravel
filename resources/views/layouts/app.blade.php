<html>
<head>
    <meta charset="utf-8"/>
    <title>App Name - @yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center pt-4 pb-4 mb-5 border-bottom">Users - CRUD</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <aside class="aside">
                <nav class="nav">
                    <ul>
                        <li><a href="{{url('user')}}">@lang('User list')</a></li>
                        <li><a href="{{url('user/create')}}">@lang('Create user')</a></li>
                    </ul>
                </nav>
            </aside>
        </div>
        <div class="col-8">
            <section class="content">
                @yield('content')
            </section>
        </div>
    </div>
</div>
</body>
</html>
