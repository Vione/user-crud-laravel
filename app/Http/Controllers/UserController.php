<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'string|min:3|max:50',
            'username'  => 'required|min:6|max:25',
            'password'  => 'required|min:8|max:80|confirmed',
            'email'     => 'required|email|unique:users,email',
        ]);

        $validator->validate();
        $response = redirect('/user/create');

        if ($validator->fails()) {
            return $response->withInput();
        }

        $user = new User($request->all());
        $saved = $user->save();

        if (!$saved) {
            return $response->withError(__('An error occurred while creating the user!'));
        }

        return $response->withSuccess(__('Successfully created user.'));
    }

    public function update(int $id)
    {
        $user = User::query()->where(['id' => $id])->first();

        if ($user === null) {
            return redirect("/user")->withError(__('User with provided id not exists!'));
        }

        return view('user.update', [
            'user' => $user,
        ]);
    }

    public function modify(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'string|min:3|max:50',
            'password'  => 'required|min:8|max:80',
            'email'     => 'required|email|unique:users,email,' . $id,
        ]);

        $validator->validate();
        $response = redirect("/user/{$id}/update");

        if ($validator->fails()) {
            return $response->withInput();
        }

        $user = User::query()->where(['id' => $id])->first();

        if ($user === null) {
            return $response->withError(__('User with provided id not exists!'));
        }

        $updated = $user->update([
            'full_name' => $request->input('full_name'),
            'email'     => $request->input('email'),
        ]);

        if (!$updated) {
            return $response->withError(__('An error occurred while updating the user!'));
        }

        return $response->withSuccess(__('Successfully updated user.'));
    }

    public function read()
    {
        $users = User::all();

        return view('user.list', [
            'users' => $users,
        ]);
    }

    public function delete(int $id)
    {
        $user = User::query()->where(['id' => $id])->first();
        $response = redirect('/user');

        if ($user === null) {
            return $response->withError(__('User with provided id not exists!'));
        }

        try {
            $deleted = $user->delete();
        } catch (\Exception $e) {
            return $response->withError(__('An error occurred while deleting the user!'));
        }

        if (!$deleted) {
            return $response->withError(__('An error occurred while deleting the user!'));
        }

        return $response->withSuccess(__('User was successfully removed.'));
    }

}
